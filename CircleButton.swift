//
//  CircleButton.swift
//  Speech Recognition
//
//  Created by Samuel Tan on 15/6/17.
//  Copyright © 2017 Samuel Tan. All rights reserved.
//

import UIKit

@IBDesignable
class CircleButton: UIButton {
    
    @IBInspectable var cornerRadius: CGFloat = 30.0 {
        didSet {
            setupView()
        }
    }

    override func prepareForInterfaceBuilder() {
        setupView()
    }

    func setupView() {
        layer.cornerRadius = cornerRadius
    }
}
